<?php

ini_set('display_errors', 1);
session_start();

// $_SESSION['loggedIn'] = 0;
require_once "../Controllers/Controller.php";

use Controllers\Controller;

$controller = new Controller();
$controller->route();

