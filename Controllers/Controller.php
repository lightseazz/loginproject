<?php

namespace Controllers;

require_once "../Models/Model.php";
use Models\Model;

class Controller
{
    private $model;

    public function __construct()
    {
        $this->model = new Model();
    }
    public function route()
    {
        $request = $_SERVER['REQUEST_URI'];
        if ($_SESSION['loggedIn']) {
            $this->routeAfterLogin($request);
        } 

        else {
            $this->routeBeforeLogin($request);

        }

    }

    private function routeBeforeLogin($request)
    {

        if ($request === '/' || $request === '/?errorLogin=1') {
            $_SESSION['loggedIn'] = "";
            include "../Views/login.php";
        }

        if ($request === '/submitLogin') {
            if ($this->checkUserLogin())
                header('Location: /homepage');
            else
                header('Location: /?errorLogin=1');

        }
    }
    private function routeAfterLogin($request)
    {
        if ($request === '/homepage') {
            include "../Views/homepage.php";
        }
        if ($request === '/logout') {
            $_SESSION['loggedIn'] = "";
            header('Location: /');

        }
    }


    private function checkUserLogin()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $isValid = $this->model->checkUserLogin($username, $password);
        if ($isValid) {
            $_SESSION['loggedIn'] = $_POST['username'];
            return true;
        }
        return false;
    }

}