<?php
namespace Models;

class Model
{

    private $dbconnect;

    public function __construct()
    {
        try {
            if (!$this->dbconnect) {
                $dbconn = new \PDO("pgsql: host=postgres; dbname=loginproject ;user=default; password=secret ");
                $dbconn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $dbconn->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
                $this->dbconnect = $dbconn;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();

        }

    }

    public function checkUserLogin($username, $password)
    {
        $query = "SELECT password from users where username='{$username}'";
        $resultQuery = $this->dbconnect->query($query)->fetchColumn();
        $result = password_verify($password, $resultQuery);
        return $result;
    }

}